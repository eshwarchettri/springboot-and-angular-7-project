package com.trimindtech.assignment.payment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class PaymentController {

	@Autowired
	private PaymentService paymentService;
	
	@GetMapping("/paymentDetails")
	public List<PaymentBean> getAllPaymentDetails(){
		return paymentService.getAllPaymentDetails();
	}
	@PostMapping("/saveDetails")
	public PaymentBean sayHello(@RequestBody PaymentBean bean) {
	return  paymentService.savePaymentDetails(bean);

	}
	
	@DeleteMapping("/deletePaymentDetail/{id}")
	public void deletePaymentDetail(@PathVariable("id") int id) {
		paymentService.deletePaymentDetail(id);
	}
	
	@GetMapping("/paymentDetail/{id}")
	public PaymentBean getPaymentDetailById(@PathVariable("id") int id) {
		return paymentService.getPaymentDetailById(id);
	}
	@PutMapping("/update/{id}")
	public PaymentBean updatePaymentDetail(@PathVariable("id") int id,@RequestBody PaymentBean bean) {
		return paymentService.updatePaymentDetails(id,bean);
	}
}
